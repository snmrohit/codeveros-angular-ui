import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatDialog, MatSnackBar} from '@angular/material';

import {User} from '../user.interface';
import {UserService} from '../user.service';
import {UserDialogComponent} from '../user-dialog/user-dialog.component';
import {UserDeleteDialogComponent} from '../user-delete-dialog/user-delete-dialog.component';

@Component({
  templateUrl: './user-list.component.html',
  styleUrls: [ './user-list.component.scss' ]
})
export class UserListComponent implements OnInit, OnDestroy {
  users: User[];
  displayedColumns = [ 'username', 'firstName', 'lastName', 'email', 'actions' ];
  loading = true;

  constructor(
    private matDialog: MatDialog,
    private userService: UserService,
    private matSnackBar: MatSnackBar) {}

  ngOnInit(): void {
    this.userService.getAll()
      .subscribe(users => {
        this.users = users;
        this.loading = false;
      });
  }

  ngOnDestroy(): void {
  }

  openAddDialog(): void {
    const dialogRef = this.matDialog.open(UserDialogComponent, {
      width: '750px',
      maxWidth: '95%',
      data: {}
    });

    dialogRef.afterClosed().subscribe((newUser: User) => {
      if (newUser) {
        this.users = [newUser, ...this.users];
        this.matSnackBar.open(`User: ${newUser.firstName} ${newUser.lastName} added`);
      }
    });
  }

  editUser(user: User) {
    const dialogRef = this.matDialog.open(UserDialogComponent, {
      width: '750px',
      maxWidth: '95%',
      data: { user }
    });

    dialogRef.afterClosed().subscribe((updated: User) => {
      if (updated) {
        const index = this.users.findIndex(t => t._id === updated._id);
        if (index > -1) {
          const currUsers = [...this.users];
          currUsers[index] = updated;
          this.users = currUsers;
        }
        this.matSnackBar.open(`User: ${updated.firstName} ${updated.lastName} updated`);
      }
    });
  }

  deleteUser(user: User) {
    const dialogRef = this.matDialog.open(UserDeleteDialogComponent, {
      data: { user }
    });

    dialogRef.afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this.userService.deleteUser(user._id)
          .subscribe(deletedUser => {
            deletedUser = user; // TODO: remove this when deleted item gets returned in request
            this.users = this.users.filter((t: User) => t._id !== deletedUser._id);
            this.matSnackBar.open(`User ${deletedUser.firstName} ${deletedUser.lastName} removed`);
          });
      }
    });
  }
}
